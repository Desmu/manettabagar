# La Manette à **BAGAR**

Permet de jouer à la **BAGAR** de Shakawah avec des boutons dans le chat Twitch.

<div align="center">
![Aperçu](manettabagar.png)
</div>

## Installation

1. Télécharger [Greasemonkey pour Firefox](https://addons.mozilla.org/fr/firefox/addon/greasemonkey/) ou [Tampermonkey pour Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo), des plugins pour le navigateur permettant d'exécuter ses propres scripts sur une page web.
2. Ouvrir ensuite le script en cliquant sur ce lien : [`manettabagar.user.js`](https://gitlab.com/Desmu/manettabagar/-/raw/main/manettabagar.user.js), le plugin devrait automatiquement proposer son installation.
3. La **Manette à BAGAR** devrait apparaître sous le chat du [stream de Shakawah](https://twitch.tv/shakawah) et dans [sa version popup](https://www.twitch.tv/popout/xurei/chat).

## A propos de la BAGAR

La **BAGAR** est un jeu pour stream Twitch conçu par [Shakawah](https://www.shakawah.com/). Il met en oeuvre [le principe des automates cellulaires](https://www.youtube.com/watch?v=muuuBtpXNQQ) dans le cadre d'un jeu de conquête de territoire. Les spectatrices et spectateurs peuvent y jouer en envoyant des commandes textuelles dans le chat Twitch sur lequel le jeu est diffusé. Rejoindre une partie leur permet de placer un pixel possédant un type défini sur un terrain carré en deux dimensions.

Lors de la partie, ce pixel cherche ensuite à étendre son terrain en prenant possession de ses voisins, et s'il rencontre des pixels contrôlés par une autre personne, la prise de possession est déterminée par le type de la zone contrôlée (si le terrain est de type "eau" et que le terrain voisin est de type "feu", le terrain de type "eau" a plus de chances de prendre possession de parties du terrain de type "feu" qui lui sont voisines pour les convertir en terrain de type "eau").

Pour influer sur la partie, les joueurs et joueuses disposent d'un ensemble de commandes textuelles leur permettant de changer le type de la zone qu'elles ou ils contrôlent. Si la zone contrôlée est de type "feu", il est ainsi possible d'envoyer la commande `!water` dans le chat pour changer le type de sa propre zone en type "eau". En fonction des règles définies dans le jeu, il peut être possible de jouer avec dix-sept types différents.

Retrouvez la **BAGAR** régulièrement sur [le stream de Shakawah](https://www.twitch.tv/shakawah).

## La Manette à BAGAR

Partant du postulat qu'envoyer des commandes textuelles dans un chat Twitch peut faire l'objet de fautes de frappe pouvant être fatales lors de parties nerveuses de **BAGAR**, la **Manette à BAGAR** (aussi nommée **MANETTABAGAR**) est un script pour navigateur web de bureau greffant un ensemble de boutons à l'interface du chat Twitch ayant pour but de faciliter l'utilisation du jeu.
Dix-neufs boutons sont ajoutés au chat, cliquer sur l'un de ces boutons permet de remplir automatiquement le champ de message Twitch avec la commande associée et de l'envoyer instantanément. Dix-sept des boutons représentent les dix-sept types ; sont également disponibles deux boutons `!join` pour envoyer la commande permettant de rejoindre une partie, et `!random` pour envoyer la commande permettant de choisir un type au hasard.
La **MANETTABAGAR** dispose de trois modes d'affichage : `small` pour un affichage compact des boutons, `large` agrandit la taille des boutons, `fullscreen` masque les éléments du chat Twitch.
Une liste de valeurs est également disponible, permettant de n'afficher que cinq, neuf ou treize des types en fonction des règles de la partie (et ainsi alléger l'interface).
L'option `Hints` permet enfin d'afficher les forces et faiblesses de chaque type au survol de la souris sur un bouton de type.

La **BAGAR** dispose également de versions abrégées des commandes (`!j` remplace `!join`, `!wat` ou `!wate` remplacent `!water`, ...) permettant de contourner une restriction de Twitch empêchant d'envoyer deux fois le même message d'affilée. La **Manette à BAGAR** poste ainsi automatiquement la version abrégée de la commande si elle doit être entrée deux fois d'affilée.

### Mode téléphone

La **MANETTABAGAR** dispose d'une option d'affichage `fullscreen`. Le chat Twitch est masqué et les boutons sont agrandis pour prendre plus de place. L'idée est ainsi de pouvoir utiliser son smartphone comme manette de jeu. Néanmoins, les conditions à suivre pour réaliser cette manoeuvre sont nombreuses :
1. Créer un [compte Firefox](https://accounts.firefox.com/).
2. Créer une [collection de modules personnalisée pour Firefox](https://addons.mozilla.org/fr/firefox/collections/) et y ajouter le plugin [Tampermonkey](https://addons.mozilla.org/fr/firefox/addon/tampermonkey/).
3. Télécharger et installer une version de développement ou alternative du navigateur Firefox pour Android (comme [Firefox Nightly](https://play.google.com/store/apps/details?id=org.mozilla.fenix) ou [Fennec F-Droid](https://f-droid.org/fr/packages/org.mozilla.fennec_fdroid/)). L'ensemble des étapes suivantes s'effectuera ensuite sur ce navigateur.
4. Activer [le menu de développement](https://blog.mozilla.org/addons/2020/09/29/expanded-extension-support-in-firefox-for-android-nightly/) du navigateur.
5. Y ajouter la collection de modules personnalisée créée à l'étape 2 en renseignant l'identifiant utilisateur et le nom de la collection.
6. Installer Tampermonkey depuis la liste des modules pour Firefox pour Android désormais actualisée.
7. Installer le script [`manettabagar.user.js`](https://gitlab.com/Desmu/manettabagar/-/raw/main/manettabagar.user.js).
8. [Se connecter à Twitch](https://www.twitch.tv/login).
9. Accéder au [chat du stream en mode fenêtré](https://www.twitch.tv/popout/shakawah/chat) et attendre la fin du chargement de tous les widgets.
10. Passer en mode "plein écran" avec le mode `fullscreen`.

## Remerciements

- [Shakawah](https://www.shakawah.com/) pour avoir crée le jeu initial sans lequel cette manette ne sert à rien.
- [xurei](https://github.com/xurei) pour avoir réalisé le CSS des indices visuels au survol des boutons et une partie du README.
- [Mahoneko](https://twitch.tv/mahoneko) pour les suggestions d'adaptations au chat WYSIWYG de Twitch.

## NO WYSIWYG

Le champ de texte Twitch a été récemment mis à jour pour intégrer éditeur `What You See Is What You Get` (permettant aux emotes de s'afficher directement dans ce champ en lieu et place de leur identifiant textuel). Ce changement modifie en profondeur le fonctionnement de ce champ, rendant l'exécution de ce script moins fluide.
Si ce script devient inutilisable, il est néanmoins possible de désactiver ce nouveau type de champ de texte en cliquant sur le bouton `NO WYSIWYG`, qui remettra en place l'ancienne version du champ de texte.
Il s'agit d'une solution temporaire, l'idée étant de faire en sorte que ce nouveau type de champ de texte soit optimalement supporté à terme, en partant du principe qu'il ne sera un jour potentiellement plus possible de rebasculer sur l'ancienne version du champ. C'est pourquoi le changement de type de champ n'est conservé que le temps d'une session pour prévenir un éventuel problème : le nouveau type de champ de texte reviendra au rechargement de la page (et il redevient alors possible de le désactiver).